#!/bin/bash
# Generates the list of videos in url in a wiki-table friendly format that
# can just be copied into the wiki

file=deb16.subs.wiki
url='http://meetings-archive.debian.net/pub/debian-meetings/2016/debconf16/'

cat << EOF > $file
{| class="wikitable"
|-
! Locked by
! Video url
EOF

curl -s $url'?C=M;O=A' |grep VID |cut -d\" -f6|
while read video; do

cat << EOF >> $file
|-
| 
EOF

	echo "| [$url$video $video]"

done >> $file

cat << EOF >> $file
|}
EOF

#
#cat << EOF >> $file
#</channel>
#EOF
