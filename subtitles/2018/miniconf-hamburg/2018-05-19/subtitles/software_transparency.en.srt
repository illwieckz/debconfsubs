1
00:00:07,760 --> 00:00:11,736
This will be an academic talk
as announced.

2
00:00:12,478 --> 00:00:18,737
I will try to bring some of my research
I did during my PhD into the real world.

3
00:00:20,967 --> 00:00:26,088
We are going to talk about the security
of software distribution and

4
00:00:26,088 --> 00:00:30,110
I'm going to propose a security feature
that adds on top of

5
00:00:30,110 --> 00:00:33,041
the signatures we have up to today

6
00:00:33,041 --> 00:00:39,708
and also the reproducible builds that
we already have to very large degree.

7
00:00:40,363 --> 00:00:47,395
I am going to highlight a few points where
I think infrastructure changes are required

8
00:00:47,395 --> 00:00:53,079
to accommodate this system and I would
also appreciate any feedback

9
00:00:53,079 --> 00:00:55,151
you might have.

10
00:00:56,402 --> 00:00:58,900
I'm going to start off
with a few motivational words

11
00:00:58,900 --> 00:01:02,600
why should we care about
the security of software distribution

12
00:01:02,601 --> 00:01:05,672
we already do have
cryptographic signatures

13
00:01:05,672 --> 00:01:11,428
I've just put up a few examples of
recent attacks that involved

14
00:01:11,428 --> 00:01:18,554
the distribution of software where
people who presumably thought

15
00:01:18,554 --> 00:01:24,288
they knew what they were doing had
grave problems with software distribution.

16
00:01:24,776 --> 00:01:28,266
For example, the juniper backdoors,
pretty famous.

17
00:01:29,323 --> 00:01:32,536
Juniper discovered two backdoors
in the code and

18
00:01:32,536 --> 00:01:38,068
nobody really knew where they were
coming from.

19
00:01:39,084 --> 00:01:42,823
Another example would be
Chrome extension developers

20
00:01:42,823 --> 00:01:48,428
who got their credentials fished and
subsequently their extensions backdoored

21
00:01:48,428 --> 00:01:57,898
or another example, a signed update to
a banking software actually included

22
00:01:57,898 --> 00:02:01,967
a malware and infected several banks.

23
00:02:03,186 --> 00:02:11,113
I hope this is motivation for us to
consider this kinds of attacks

24
00:02:11,113 --> 00:02:14,973
to be possible and to prepare ourselves.

25
00:02:16,629 --> 00:02:20,827
I have two main goals in the system
I am going to propose.

26
00:02:21,193 --> 00:02:24,649
The first is to relax trust in the archive.

27
00:02:24,893 --> 00:02:31,361
In particular, what I want to achieve is
a level of security even if

28
00:02:31,361 --> 00:02:38,185
the archive is compromised and
the specific thing I am going to do is

29
00:02:38,185 --> 00:02:41,161
to detect targeted backdoors.

30
00:02:41,447 --> 00:02:46,974
That means backdoors that are distributed
only to a subset of the population and

31
00:02:46,974 --> 00:02:53,235
what we can achieve is to force
the attacker to deliver the malware

32
00:02:53,235 --> 00:02:59,744
to everybody, thereby greatly decreasing
their degree of stealth and increasing

33
00:02:59,744 --> 00:03:02,462
their danger of detection.

34
00:03:03,360 --> 00:03:05,434
This would work to our advantage.

35
00:03:06,167 --> 00:03:10,113
The second goal is the forensic auditability

36
00:03:10,113 --> 00:03:16,945
which overlaps to a surprising degree
with the first one in technical terms,

37
00:03:16,945 --> 00:03:19,017
in terms of implementation.

38
00:03:20,199 --> 00:03:23,771
So, what I want to ensure is that we have

39
00:03:23,771 --> 00:03:27,025
inspectable source code for every binary.

40
00:03:27,758 --> 00:03:32,720
We do have of course the source code
available from our packages, but

41
00:03:32,720 --> 00:03:39,307
only for the most recent version,
everything else is a best effort

42
00:03:39,307 --> 00:03:43,363
by the code archiving services.

43
00:03:44,348 --> 00:03:51,090
The mapping between those and binary
can be verified once we have

44
00:03:51,090 --> 00:03:53,862
reproducible builds to a large extent.

45
00:03:55,325 --> 00:04:01,133
I want to make sure that we can identify
the maintainer responsible for distribution

46
00:04:01,133 --> 00:04:08,085
of a particular package and the system
is also interested in providing

47
00:04:08,085 --> 00:04:10,605
attribution of where something went from,

48
00:04:10,931 --> 00:04:16,375
so that we are not in a situation where we
notice something went wrong but

49
00:04:16,375 --> 00:04:22,069
we don't really know where we have to look
in order to find the problems

50
00:04:22,069 --> 00:04:28,691
but that we really have specific and
secured indication of

51
00:04:28,691 --> 00:04:31,896
where a compromised problem
was coming from.

52
00:04:33,649 --> 00:04:36,611
Let's quickly recap how our software
distribution works.

53
00:04:37,017 --> 00:04:41,804
We have the maintainers who upload
their code to the archive.

54
00:04:42,463 --> 00:04:47,284
The archive has access to a signing key
which signs the releases.

55
00:04:47,658 --> 00:04:52,329
Actually, metadata covering all the actual
binary packages.

56
00:04:53,022 --> 00:04:57,163
These are distributed over
the mirror network

57
00:04:57,163 --> 00:05:02,210
from where the apt clients will download
the package metadata.

58
00:05:02,489 --> 00:05:07,165
That means the hash sums for the packages,
their dependencies and so on

59
00:05:07,165 --> 00:05:09,925
as well as the actual packages themselves.

60
00:05:12,246 --> 00:05:18,993
This central architecture has an important
advantage,

61
00:05:18,993 --> 00:05:23,345
mainly the mirror network need not
to be trusted, right?

62
00:05:23,345 --> 00:05:29,156
We have the signature that covers all
the contents of binary and source packages

63
00:05:29,156 --> 00:05:32,726
and the metadata, so the mirror network
need not to be trusted.

64
00:05:33,020 --> 00:05:38,955
On the other hand, it makes the archive and
its signing key a very interesting target

65
00:05:38,955 --> 00:05:45,743
for attackers because this central point
controls all the signing operations.

66
00:05:46,349 --> 00:05:52,042
So this is a place where we need to be
particularly careful and perhaps

67
00:05:52,042 --> 00:05:56,062
maybe even do better than
cryptographic signatures.

68
00:05:56,508 --> 00:06:02,361
This is where the main focus of this talk
will be, although I will also consider

69
00:06:02,361 --> 00:06:05,162
the uploaders to some extent.

70
00:06:06,911 --> 00:06:08,373
We want to achieve two things:

71
00:06:08,373 --> 00:06:12,358
resistance against key compromise and
targeted backdoors and

72
00:06:12,358 --> 00:06:17,512
to get some better support for auditing
in case things go wrong.

73
00:06:18,450 --> 00:06:21,580
The approach that we choose to do this is

74
00:06:21,580 --> 00:06:26,542
we want to make sure that everybody runs
exactly the same software

75
00:06:26,542 --> 00:06:29,511
or at least the parts of it these choose
to install.

76
00:06:30,649 --> 00:06:35,000
If we think about that for a moment,
this gives us a number of advantages.

77
00:06:35,447 --> 00:06:40,160
For example, all the analysis that's done
on a piece of software immediately

78
00:06:40,160 --> 00:06:43,653
carries over to all other users of
the software, right?

79
00:06:44,384 --> 00:06:48,445
Because if we haven't made sure that
everybody installs the same software,

80
00:06:48,445 --> 00:06:51,423
they might not have exactly
the same version and perhaps

81
00:06:51,423 --> 00:06:53,292
some backdoored version.

82
00:06:54,302 --> 00:07:00,523
This also ensures that we cannot suffer
targeted backdoors by increasing

83
00:07:00,523 --> 00:07:03,735
the detection risk of attackers

84
00:07:03,735 --> 00:07:08,939
and we also want to have a cryptographic
proof of where something went wrong.

85
00:07:11,445 --> 00:07:19,173
Now, to look at some pictures,
I will present the data structure that

86
00:07:19,173 --> 00:07:21,940
we use in order to achieve these goals.

87
00:07:22,787 --> 00:07:28,365
The data structure is a hash tree,
a Merkle tree which is

88
00:07:28,365 --> 00:07:30,721
a data structure that operates over a list.

89
00:07:30,997 --> 00:07:35,353
So we have a list of these squares here
which represent the list items.

90
00:07:35,353 --> 00:07:39,242
In our case, this is going to be
the files containing a package metedata

91
00:07:39,242 --> 00:07:41,966
that just dependencies, a hash sum of
packages

92
00:07:41,966 --> 00:07:46,730
and also the source packages themselves
are going to be elements in this list.

93
00:07:47,787 --> 00:07:49,208
The tree works as follows.

94
00:07:49,982 --> 00:07:52,222
It uses a cryptographic hash function

95
00:07:52,222 --> 00:07:55,838
which is a collision resistant compressing
function

96
00:07:55,838 --> 00:08:00,718
and the labels of the inner nodes
of the tree are computed as

97
00:08:00,718 --> 00:08:04,661
the hashes of the children. Ok?

98
00:08:05,884 --> 00:08:10,028
Once we have computed the root hash,
the root label,

99
00:08:10,028 --> 00:08:14,827
we have fixed all the elements and
none of the elements can be changed

100
00:08:14,827 --> 00:08:16,658
without changing the root hash.

101
00:08:18,201 --> 00:08:22,390
We can exploit this in order to
efficiently prove

102
00:08:22,390 --> 00:08:25,973
the two following properties for elements.

103
00:08:26,338 --> 00:08:31,056
First of all, we can efficiently prove
the inclusion of a given element

104
00:08:31,056 --> 00:08:32,156
in the list.

105
00:08:32,442 --> 00:08:37,116
If we know the tree root ex ante,
this works as follows:

106
00:08:37,221 --> 00:08:41,412
let's make a quick example, we see
the third list item is marked with an X

107
00:08:41,412 --> 00:08:49,829
and if I know the tree root, then
the server operating the tree structure

108
00:08:49,829 --> 00:08:54,632
will only need to give me the three gray
marked labels,

109
00:08:54,632 --> 00:09:01,416
the three marked node values and then
I can recompute the root hash and

110
00:09:01,416 --> 00:09:06,216
be convinced that this element actually
was contained in the list.

111
00:09:06,624 --> 00:09:12,281
The second property is that we can also
efficiently verify the append-only operation

112
00:09:12,281 --> 00:09:13,775
of the list.

113
00:09:14,051 --> 00:09:17,764
So we can have a log server operating
this kind of structure and

114
00:09:17,764 --> 00:09:19,432
the log server need not to be trusted,

115
00:09:19,432 --> 00:09:23,817
it's not going to be trusted third party
but rather, its operation can be

116
00:09:23,817 --> 00:09:25,766
verified from the outside.

117
00:09:28,123 --> 00:09:31,019
So, what does this design look like?

118
00:09:31,505 --> 00:09:35,855
The theoretical foundation is called
a transparency overlay and

119
00:09:35,855 --> 00:09:38,006
in our system it looks like this:

120
00:09:38,006 --> 00:09:40,565
We have the archive as per usual,

121
00:09:40,565 --> 00:09:46,533
we have a log server and the archive will
submit package metadata, the release file,

122
00:09:46,533 --> 00:09:52,135
the packages file containing dependencies
and so on and the source code

123
00:09:52,135 --> 00:09:54,572
into this log server.

124
00:09:56,403 --> 00:10:03,800
The apt client will be augmented with
an auditor component and

125
00:10:03,800 --> 00:10:09,654
this auditor component is responsible for
verifying the correct log operation

126
00:10:09,654 --> 00:10:15,463
as well as the inclusion of the downloaded
release into the log.

127
00:10:15,991 --> 00:10:21,155
This is a mechanism which we will be able
to make sure that everybody is running

128
00:10:21,155 --> 00:10:25,134
the exact same version of the software
they installed.

129
00:10:27,089 --> 00:10:29,267
A third component is the monitor.

130
00:10:29,562 --> 00:10:37,446
The monitor is necessary also to verify
log operation and also to inspect

131
00:10:37,446 --> 00:10:42,127
the elements that are contained in the log

132
00:10:44,281 --> 00:10:52,161
The monitor would then be run by groups
of individuals or individuals that want to

133
00:10:52,161 --> 00:10:57,296
make sure of certain properties in the log

134
00:11:00,748 --> 00:11:05,791
Alright, let's quickly recap.

135
00:11:06,565 --> 00:11:12,816
We have added this log server, which
can prove two properties efficiently

136
00:11:12,816 --> 00:11:15,739
to the outside world.

137
00:11:17,075 --> 00:11:20,504
And we have the auditor and monitor
components.

138
00:11:21,155 --> 00:11:27,045
The auditor is added to the apt client
and the monitor does

139
00:11:27,045 --> 00:11:29,772
additional investigative tasks.

140
00:11:31,168 --> 00:11:38,344
Now, in order to make this system work,
we need to…

141
00:11:38,920 --> 00:11:40,833
I need to make a few assumptions.

142
00:11:41,239 --> 00:11:47,893
The archive will need to handle
log submission and distribution of

143
00:11:47,893 --> 00:11:51,104
certain log datastructure.

144
00:11:51,712 --> 00:11:56,412
These are usually very small things
given to the archive

145
00:11:56,412 --> 00:11:58,128
in response to submission.

146
00:11:58,737 --> 00:12:04,260
Then I'm assuming a very consistent
release frequency.

147
00:12:05,358 --> 00:12:11,127
The archive is responsible for distributing
reproducible binaries

148
00:12:11,127 --> 00:12:13,078
in my architecture.

149
00:12:13,609 --> 00:12:20,349
I'm assuming that the buildinfo files are
covered by the release file

150
00:12:21,041 --> 00:12:27,990
I treat them as additional source metadata
so whenever the source package or

151
00:12:27,990 --> 00:12:34,578
the buildinfo file changes, I expect
an increase in the binary version number.

152
00:12:35,636 --> 00:12:42,092
I also assume source-only uploads and
one additional thing that we have,

153
00:12:42,092 --> 00:12:48,965
keyring source package treated
by the archive as authoritative and

154
00:12:48,965 --> 00:12:53,105
this keyring must have
the special property that

155
00:12:53,105 --> 00:12:57,772
is operated in append-only so that
we can go back in time and see

156
00:12:57,772 --> 00:13:00,505
what keys were authorized at different
points in time.

157
00:13:03,877 --> 00:13:11,115
The log server is a standalone server
component that speaks at the moment on

158
00:13:11,115 --> 00:13:13,147
an HTTP-based protocol.

159
00:13:14,003 --> 00:13:17,419
Probably one would want to have
more than one, but

160
00:13:17,419 --> 00:13:21,150
we are going to have, I think,
a much easier time running log servers

161
00:13:21,150 --> 00:13:24,250
than for example, the certificate
transparency people

162
00:13:24,250 --> 00:13:29,816
because we only have one source
of writing access,

163
00:13:29,816 --> 00:13:36,348
namely the archive, so we can easily
schedule the write access,

164
00:13:36,348 --> 00:13:41,064
and you can have read-only frontends that
aren't quite critical.

165
00:13:43,219 --> 00:13:50,060
The auditor component would need to be
integrated into the apt client or library.

166
00:13:50,369 --> 00:13:53,508
It needs two things like cryptographic
verifications,

167
00:13:53,508 --> 00:13:59,971
understand a bit more file formats and
some more network access.

168
00:14:02,531 --> 00:14:06,594
Parts of the proof could also
probably distribute over

169
00:14:06,594 --> 00:14:12,284
the mirror network and we need
not necessarily do everything

170
00:14:12,611 --> 00:14:15,210
in live communication with
the log server.

171
00:14:18,656 --> 00:14:23,129
So, this covers archive auditor and
log server.

172
00:14:23,908 --> 00:14:31,342
The monitoring servers have a few functions
that are necessary for the verification of

173
00:14:31,342 --> 00:14:37,517
the log itself, meaning that they verify
the append-only operation of the log

174
00:14:37,517 --> 00:14:43,333
and they will also likely want to exchange
the tree roots with perhaps

175
00:14:43,333 --> 00:14:45,316
other monitors and some auditors.

176
00:14:46,535 --> 00:14:51,128
The important verification functions
of the log server are validating

177
00:14:51,128 --> 00:14:56,771
the metadata of the release packages and
sources file,

178
00:14:56,771 --> 00:15:01,484
namely making sure that these are complete,
that the sources are available,

179
00:15:01,484 --> 00:15:05,863
that the versions are incremented
correctly and so on.

180
00:15:06,196 --> 00:15:10,222
And that's necessary to make sure that
a compromised archive can't do

181
00:15:10,222 --> 00:15:11,808
certain attacks.

182
00:15:12,702 --> 00:15:19,041
Also in this category is the fact that
we depend on a fixed release frequency

183
00:15:19,041 --> 00:15:26,837
and monitors will also be verifying
the upload ACL,

184
00:15:26,837 --> 00:15:29,276
meaning which keys are authorized to
upload.

185
00:15:30,406 --> 00:15:36,633
Monitors also would be verifying
reproducible builds in this scenario.

186
00:15:40,784 --> 00:15:48,905
That's the monitoring functions and
I think that many different people and

187
00:15:48,905 --> 00:15:55,769
groups in Debian could get some benefits
out of these monitoring functions

188
00:15:55,769 --> 00:16:00,560
in order to verify that everything
worked correctly.

189
00:16:01,331 --> 00:16:05,477
We should note that all these verifications
are completely independent of

190
00:16:05,477 --> 00:16:09,379
the existing infrastructure because
happening on the client side.

191
00:16:09,580 --> 00:16:15,835
So we don't depend on any notifications
from the existing infrastructure that

192
00:16:15,835 --> 00:16:19,077
works correctly and no notifications
are stopped.

193
00:16:19,370 --> 00:16:22,988
This can be done completely
on the client side using

194
00:16:22,988 --> 00:16:26,119
the data provided by the log server.

195
00:16:26,724 --> 00:16:35,224
For example, maintainers could verify that
the code uploaded builds reproducibly

196
00:16:35,224 --> 00:16:38,438
using the corresponding build info or

197
00:16:38,438 --> 00:16:40,347
they could have checks:

198
00:16:40,347 --> 00:16:43,037
which uploads were done using their key

199
00:16:43,037 --> 00:16:47,503
which packages were modified perhaps
by other people

200
00:16:47,503 --> 00:16:53,643
the keyring maintainers or account
managers could be looking at

201
00:16:53,643 --> 00:16:59,081
the keyring: what keys are in the keyring
and what uploads were done

202
00:16:59,081 --> 00:17:00,703
using which keys.

203
00:17:02,163 --> 00:17:12,284
And the archive, last but not least, has
an additional verification step available

204
00:17:12,284 --> 00:17:18,280
to make sure all the metadata was produced
correctly and to know

205
00:17:18,280 --> 00:17:22,882
wierd things happened during the production
of a given release.

206
00:17:27,530 --> 00:17:29,253
This thing actually exists.

207
00:17:29,253 --> 00:17:33,427
Well, I have programmed prototypes
for all these components,

208
00:17:34,281 --> 00:17:37,451
meaning nothing that would be ready
to implement,

209
00:17:37,451 --> 00:17:39,562
but to show that it actually works.

210
00:17:40,406 --> 00:17:46,144
I've used two years of Debian Stretch
releases and fed it into the system.

211
00:17:46,752 --> 00:17:53,386
This resulted in a tree size of
270,000 elements and

212
00:17:53,386 --> 00:17:58,512
the storage required was about 400GB
where almost all of that is

213
00:17:58,512 --> 00:18:00,013
source packages.

214
00:18:00,950 --> 00:18:04,970
I would say that it's imminently feasible
to do this.

215
00:18:04,970 --> 00:18:09,843
The monitor functions run rather cheaply.

216
00:18:10,454 --> 00:18:17,444
A monitor needs not necessarily to keep
a complete copy of the log in all cases

217
00:18:19,274 --> 00:18:25,954
but what I noticed some unexpected events
in the package metadata.

218
00:18:27,538 --> 00:18:33,063
I have observed sources missing and
version increments missing where

219
00:18:33,063 --> 00:18:36,356
I think there should be a version increment

220
00:18:36,960 --> 00:18:42,164
So I'll be looking more closely
into these cases.

221
00:18:45,307 --> 00:18:50,946
If anybody is interested at
the theoretical side of this,

222
00:18:50,946 --> 00:18:55,259
this would be the immediate pointers
I can give.

223
00:18:56,266 --> 00:19:00,818
The first paper is the theoretical and
mathematical foundation and

224
00:19:00,818 --> 00:19:09,043
the other ones are applications of
similar transparency work, but

225
00:19:09,043 --> 00:19:14,488
with different goals.

226
00:19:18,155 --> 00:19:25,893
Summarizing, we can introduce a system
to detect target backdoors,

227
00:19:25,893 --> 00:19:29,469
even under compromise of the archive.

228
00:19:29,998 --> 00:19:36,462
We need to add a bit more infrastructure
and need to change how some things are done

229
00:19:37,927 --> 00:19:46,990
We also can improve the auditability of
what we can securely identify when

230
00:19:46,990 --> 00:19:49,394
things go wrong.

231
00:19:50,367 --> 00:19:55,727
In particular, we can make sure that for
every binary, we can get

232
00:19:55,727 --> 00:20:02,589
the source code that was used to produced
the binary

233
00:20:02,589 --> 00:20:06,289
and then identify
the responsible maintainer.

234
00:20:06,940 --> 00:20:10,590
There's one class of attacks I have left
out for today,

235
00:20:10,590 --> 00:20:14,450
if anybody wants to talk about that, we
can do so too.

236
00:20:15,598 --> 00:20:20,677
And now, I'm interested in your questions
and feedback.

237
00:20:23,354 --> 00:20:28,083
[Applause]

238
00:20:34,261 --> 00:20:39,828
[Q] Did you already test the reproducibility
and how do you interact with

239
00:20:39,828 --> 00:20:43,652
problems of not reproducible packages?

240
00:20:43,922 --> 00:20:46,577
I mean, do you not integrate some
into the log?

241
00:20:47,920 --> 00:20:53,448
[A] For now, the implementation of
my monitor functions hasn't covered

242
00:20:53,448 --> 00:20:54,944
reproducibility.

243
00:20:55,276 --> 00:21:01,130
I think the first step to do so would be
to have a blacklist of packages

244
00:21:01,130 --> 00:21:05,959
that are known not to be built reproducibly
and then try to get on with it.

245
00:21:16,668 --> 00:21:18,011
[Q] Two questions.

246
00:21:18,415 --> 00:21:20,970
You say "authenticating metadata and
code".

247
00:21:21,294 --> 00:21:25,683
This means signing or what is it exactly,
"authenticating"?

248
00:21:26,415 --> 00:21:27,872
[A] At which point?

249
00:21:28,649 --> 00:21:32,684
[Q] It was… back. Where the tree is.

250
00:21:33,579 --> 00:21:36,588
Yes, yes. The tree before that.

251
00:21:39,517 --> 00:21:40,448
[A] Ok.

252
00:21:41,671 --> 00:21:49,238
This authentication here doesn't quite mean
a signature.

253
00:21:49,725 --> 00:21:57,166
It means if I know the value of the root
of the hashtree, then

254
00:21:57,166 --> 00:22:04,196
I can be assured that a given element
is included if I'm told

255
00:22:04,196 --> 00:22:10,984
the value of the three gray marked
inner nodes here.

256
00:22:11,311 --> 00:22:15,510
And that works by recomputing
the hash tree.

257
00:22:19,689 --> 00:22:23,720
[Q] Ok, I think I have to defer this
to after the talk.

258
00:22:24,492 --> 00:22:26,401
[A] Yeah, I can explain.

259
00:22:26,921 --> 00:22:28,761
[Q] Another question would be,

260
00:22:28,761 --> 00:22:31,525
so, detection of targeted backdoors.

261
00:22:32,905 --> 00:22:39,205
You mean at the stage of signing archive
or which backdoors?

262
00:22:40,625 --> 00:22:46,360
[A] The scenario would be that
the signing key of the archive is

263
00:22:46,360 --> 00:22:52,665
used to create an additional release file
which covers

264
00:22:52,665 --> 00:22:54,577
a manipulated software version.

265
00:22:54,982 --> 00:22:59,652
And this software version and signature is
only shown to the victim population

266
00:22:59,652 --> 00:23:01,927
and not to the general population.

267
00:23:02,950 --> 00:23:08,224
This means that the malicious software
would only be observed by the victim

268
00:23:08,224 --> 00:23:09,892
and not by everybody else.

269
00:23:10,585 --> 00:23:14,568
My goal is to force the attacker to
distribute the malicious software

270
00:23:14,568 --> 00:23:18,020
to the whole world in order to increase

271
00:23:18,020 --> 00:23:22,242
the chance that they're going to be
detected and thereby deterring perhaps

272
00:23:22,242 --> 00:23:24,359
the attack from the beginning.

273
00:23:39,693 --> 00:23:41,679
[Q] Great talk. Great ideas as well.

274
00:23:43,791 --> 00:23:46,598
I really liked your slide on
your assumptions

275
00:23:47,198 --> 00:23:49,608
???
honest about them like

276
00:23:49,608 --> 00:23:51,191
"yeah we assume all these things"

277
00:23:52,732 --> 00:23:55,701
I wouldn't underestimate how difficult
it would be to make

278
00:23:55,701 --> 00:23:57,082
some of these changes.

279
00:23:57,082 --> 00:24:00,900
I mean, even ones that look simple, like
source-only uploads.

280
00:24:01,143 --> 00:24:02,812
Everyone wants them, right?

281
00:24:05,349 --> 00:24:11,498
[A] Yes, sure, we have to start somewhere
and I hope if people are convinced that

282
00:24:11,702 --> 00:24:14,307
this is a great idea and we should to this

283
00:24:14,307 --> 00:24:18,288
then we get some more impetus
for these things that everybody wants

284
00:24:18,288 --> 00:24:20,522
like source-only uploads.

285
00:24:21,750 --> 00:24:25,001
[Q] Thank you, yeah, and it will be really
pretty good to base this stuff

286
00:24:25,001 --> 00:24:29,431
on reproducible builds effort because
it builds on the same choices.

287
00:24:29,919 --> 00:24:31,427
Thank you.

288
00:24:33,952 --> 00:24:37,407
[A] Yeah, so I'm interested in any kind
of feedback.

289
00:24:37,735 --> 00:24:42,818
If you think it's a great idea or think
there are some problems I might have missed

290
00:24:42,818 --> 00:24:45,791
or it might get difficult to implement.

291
00:24:47,702 --> 00:24:51,559
Please come talk to me in case you have
anything.

292
00:24:53,070 --> 00:24:58,631
[Applause]
