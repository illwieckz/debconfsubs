1
00:00:00,260 --> 00:00:06,321
[Talkmeister]: Welcome, our next talk will
be about the Debian Long Term support

2
00:00:06,321 --> 00:00:09,981
team and the speaker is
Raphaël Hertzog.

3
00:00:11,340 --> 00:00:12,481
[Raphaël Hertzog]: Hello.

4
00:00:13,701 --> 00:00:16,741
Today I will speak a bit about Debian
long term support.

5
00:00:17,080 --> 00:00:19,801
I guess most of you already know about
it.

6
00:00:20,241 --> 00:00:23,962
Are there some people who have no
idea what this is about?

7
00:00:24,841 --> 00:00:26,202
No, good.

8
00:00:30,901 --> 00:00:34,461
I will make my talk in 3 parts.

9
00:00:35,060 --> 00:00:38,221
First I will present the team, how it
works

10
00:00:38,221 --> 00:00:45,861
I will give some facts about how it
evolved over the first years.

11
00:00:47,361 --> 00:00:50,662
I took some time to collect statistics
and believe they are rather interesting

12
00:00:53,181 --> 00:00:54,621
I will also speak about the future

13
00:00:55,121 --> 00:00:59,902
but there will be a separate discussion
about this in a BoF later this week.

14
00:01:01,401 --> 00:01:06,901
I will show you how to help because, like
any other team in Debian it is open

15
00:01:06,901 --> 00:01:10,161
to anyone. We welcome help.

16
00:01:11,081 --> 00:01:13,321
At the end I will answer your questions.

17
00:01:15,961 --> 00:01:20,300
What is LTS about?

18
00:01:29,801 --> 00:01:31,041
The idea is really simple.

19
00:01:31,842 --> 00:01:37,461
We wanted to extend the support period
of all Debian releases.

20
00:01:39,020 --> 00:01:43,801
Currently it is basically for 1 year after
the next stable release comes out.

21
00:01:44,381 --> 00:01:50,501
We wanted to extend this to 5 years to
match, at least, Ubuntu's offering.

22
00:01:51,381 --> 00:01:58,861
which is not our competitor, but for the
companies that are making choices

23
00:01:59,020 --> 00:02:03,702
it is one of the important criteria.
So we wanted to do as well.

24
00:02:09,921 --> 00:02:15,981
Since we publish new stable releases
every 2 years it is roughly 3 years.

25
00:02:19,320 --> 00:02:23,941
A nice side benefit is that the user can
skip a full release.

26
00:02:26,041 --> 00:02:32,260
We don't officially support dist-upgrade
over going from Debian 6 to 8

27
00:02:33,382 --> 00:02:38,221
but you can do 2 dist-upgrades at
the same time, limiting the downtime

28
00:02:38,221 --> 00:02:40,541
to once every 5 years.

29
00:02:42,921 --> 00:02:49,142
By the way, in practice, in simple server
configurations, dist-upgrades tend to

30
00:02:49,280 --> 00:02:54,081
work rather well even across 2 releases.

31
00:03:00,081 --> 00:03:04,361
Keeping a distribution secure for 5 years
is a real challenge.

32
00:03:05,601 --> 00:03:09,121
It is hard work that not everybody is
willing to do.

33
00:03:10,821 --> 00:03:17,001
In Debian all the work is done by
volunteers who do the work they enjoy.

34
00:03:17,361 --> 00:03:22,901
Generally we enjoy working on new
features on top of latest releases

35
00:03:22,901 --> 00:03:28,761
and not really on backporting patches to
crud that was written 5 years ago.

36
00:03:31,780 --> 00:03:38,022
The security team has limited resources
so we could not just ask the security

37
00:03:38,022 --> 00:03:41,061
team to now do twice the work.

38
00:03:42,161 --> 00:03:49,122
But they were still really interested in
the project and wanted to support the idea

39
00:03:49,441 --> 00:03:52,361
and really helped to get it bootstrapped.

40
00:03:54,901 --> 00:03:58,781
The security team has a slightly larger
scope.

41
00:03:59,560 --> 00:04:06,361
They support all architectures, which
means you have lots of problems of

42
00:04:06,361 --> 00:04:11,261
coordination when security updates do
not compile and stuff like that.

43
00:04:12,820 --> 00:04:14,102
What did we do?

44
00:04:14,901 --> 00:04:22,062
We restricted the scope by picking
the 2 most popular architectures

45
00:04:22,062 --> 00:04:24,821
that most users care about.

46
00:04:26,601 --> 00:04:31,800
We have had some demand for ARM
architectures but up to now we only

47
00:04:31,800 --> 00:04:35,621
support amd64 and i386.

48
00:04:37,020 --> 00:04:40,942
We also excluded some packages from
security support.

49
00:04:41,400 --> 00:04:50,241
Either because they are taking too much
time, like a security issue every 2 weeks

50
00:04:54,811 --> 00:05:02,581
or that upstream is not cooperative
enough to be able to support it.

51
00:05:04,380 --> 00:05:08,581
This list was basically made by the
current security team based on their

52
00:05:08,581 --> 00:05:12,721
experience of doing security support.

53
00:05:17,021 --> 00:05:20,240
If you look at the list there are some
important restrictions.

54
00:05:20,961 --> 00:05:33,321
There's no xen, no kvm, no rails,
no browser. It sucks a bit.

55
00:05:33,681 --> 00:05:35,941
But it's a way to get it started.

56
00:05:37,181 --> 00:05:39,900
I think we can do better for wheezy.

57
00:05:41,700 --> 00:05:48,501
Basically there is no virtualization
support on the host, only on the guest.

58
00:05:54,201 --> 00:05:58,861
The security team helped to bootstrap
the LTS team but it is not the same team.

59
00:05:59,880 --> 00:06:03,881
Obviously there are members of the
security team who also work on the LTS

60
00:06:03,881 --> 00:06:09,080
team. They work in close collaboration.

61
00:06:09,982 --> 00:06:14,241
We have regular contact and they watch our
mailing lists etc.

62
00:06:14,862 --> 00:06:18,221
But the policies are different and the
infrastructure is separate,

63
00:06:19,681 --> 00:06:23,301
which is a problem but I will talk about
that later.

64
00:06:23,701 --> 00:06:27,481
We have a dedicated mailing list
debian-lts@lists.debian.org

65
00:06:28,673 --> 00:06:31,142
and a dedicated IRC channel as well.

66
00:06:31,441 --> 00:06:33,541
You are welcome to subscribe and to
join.

67
00:06:39,702 --> 00:06:42,480
It's a new team which means new people
and new members.

68
00:06:42,800 --> 00:06:44,221
Where do they come from?

69
00:06:46,681 --> 00:06:52,081
The first idea was to get help from
people in various companies

70
00:06:52,281 --> 00:06:55,281
who are already doing such in-house
support.

71
00:06:55,822 --> 00:07:04,601
We had contact with EDF, and still have,
but they were one of the first

72
00:07:04,601 --> 00:07:07,421
companies who were pushing for this
because they basically said

73
00:07:07,421 --> 00:07:11,141
we are doing this already and we can
share with other companies.

74
00:07:11,701 --> 00:07:15,761
The idea was to pool security support of
multiple companies.

75
00:07:16,722 --> 00:07:19,701
We sent a press release asking
companies to join.

76
00:07:19,961 --> 00:07:22,041
We had a few responses.

77
00:07:23,761 --> 00:07:28,522
But I'll come back later to how it evolved
It's not really satisfying.

78
00:07:29,021 --> 00:07:34,081
The other thing that we did is that we
offered companies the option to

79
00:07:34,261 --> 00:07:41,321
fund the project to bring money and use
this to pay the work of

80
00:07:41,321 --> 00:07:47,661
actual Debian contributors to do the
security updates that we need.

81
00:07:49,441 --> 00:07:56,261
We have wiki pages listing all the ways
that companies can help with money.

82
00:08:00,751 --> 00:08:06,741
In practice, most of the (wanting to be)
paid contributors joined together

83
00:08:07,400 --> 00:08:12,841
under a single offer managed by
Freexian SARL which is my own company.

84
00:08:15,861 --> 00:08:19,301
I'll quickly explain how this works.

85
00:08:22,882 --> 00:08:31,401
Most companies don't want to bother
bringing human resources

86
00:08:34,130 --> 00:08:38,560
They buy long term support contracts
from Freexian.

87
00:08:40,421 --> 00:08:49,261
We have a rate. When you give €85 you
fund 1 hour of LTS work.

88
00:08:51,601 --> 00:08:53,340
This is the current list of sponsors.

89
00:08:55,001 --> 00:09:01,382
Top level gold sponsors sponsoring
more than 1 day of work per month.

90
00:09:09,162 --> 00:09:13,541
On the other side we have Debian 
contributors that are doing the work

91
00:09:13,541 --> 00:09:16,761
and Freexian is paying them. There is a
small difference between the rate

92
00:09:16,761 --> 00:09:22,520
to cover administration costs because I
have to handle the invoices

93
00:09:22,520 --> 00:09:25,501
and some customers are using Paypal
which is taking a cut.

94
00:09:33,761 --> 00:09:37,901
We ask contributors to follow some rules.

95
00:09:38,902 --> 00:09:43,180
There is a requirement to publish a
monthly report on work done

96
00:09:43,180 --> 00:09:47,601
on paid time. So they won't get paid until
they have published a report.

97
00:09:48,721 --> 00:09:53,041
So everybody can know how the money
has been spent.

98
00:10:01,081 --> 00:10:05,441
Currently we have 7 Debian contributors
and about 30 sponsors.

99
00:10:08,921 --> 00:10:10,201
Some figures.

100
00:10:11,321 --> 00:10:17,861
Who uploaded packages?
How has it evolved since June last year?

101
00:10:18,282 --> 00:10:20,401
How is the funding evolving?

102
00:10:20,721 --> 00:10:23,561
I just updated those figures a few
days ago.

103
00:10:24,840 --> 00:10:28,360
I used this talk before at the mini
DebConf in Lyon in March,

104
00:10:28,700 --> 00:10:30,881
but I updated it again.

105
00:10:34,581 --> 00:10:42,781
The number of uploads is roughly over
one year since we started last year.

106
00:10:46,521 --> 00:10:53,061
Over 300 uploads so it is not so much
but it is almost 1 per day.

107
00:10:54,221 --> 00:10:55,960
So it is significant work.

108
00:10:57,602 --> 00:11:07,221
I have given a state here of who paid
for the work and who did it on the left

109
00:11:09,260 --> 00:11:21,761
The sponsors of Freexian are paying for
most of the uploads.

110
00:11:21,922 --> 00:11:25,601
None is a separate category grouping all
Debian maintainers.

111
00:11:25,900 --> 00:11:31,761
There are maintainers who are taking
care of their own packages in LTS.

112
00:11:33,521 --> 00:11:39,301
Security team is members of the security
team who also work within the LTS team.

113
00:11:40,240 --> 00:11:42,261
EDF is Électricité de France

114
00:11:42,541 --> 00:11:48,561
Individuals are Debian developers that
have listed themselves as members of

115
00:11:48,561 --> 00:11:55,141
the LTS team and did uploads for packages
of other maintainers not their own.

116
00:11:56,101 --> 00:11:59,180
Credativ is a German company that you
probably know.

117
00:12:01,020 --> 00:12:03,860
They have a booth here if you want a
job.

118
00:12:05,801 --> 00:12:11,001
Toshiba, Univention, Catalyst etc
are other lower figures.

119
00:12:13,281 --> 00:12:21,301
On the right are people. The top 5 people
are paid by Freexian.

120
00:12:21,541 --> 00:12:24,521
Raphaël Geissert is working for EDF.

121
00:12:27,041 --> 00:12:30,321
Thijs is a member of the security team.

122
00:12:31,561 --> 00:12:37,700
Kurt is openssl maintainer so he maintains
his own packages.

123
00:12:38,021 --> 00:12:38,721
He has a lot of work.

124
00:12:39,821 --> 00:12:41,661
Mike Gabriel is also paid by Freexian.

125
00:12:42,161 --> 00:12:47,861
Christoph Bieldl is mainly maintaining
the debian-security-support in Squeeze LTS

126
00:12:48,641 --> 00:12:51,280
Nguyen Cong is employed by Toshiba.

127
00:12:53,461 --> 00:12:59,760
Christoph Berg is employed by credativ
doing PostGreSQL maintainance.

128
00:13:03,021 --> 00:13:05,122
How did it evolve over the year?

129
00:13:06,741 --> 00:13:08,381
Again it is by affiliation.

130
00:13:08,721 --> 00:13:12,321
The big blue part is paid contributors

131
00:13:14,900 --> 00:13:22,861
You don't see it very well but the part
about maintainers is this one [points]

132
00:13:23,061 --> 00:13:28,141
It tends to do better over the months
because here we started to

133
00:13:28,141 --> 00:13:33,681
contact maintainers every time that we
have a new upload coming up

134
00:13:33,681 --> 00:13:41,121
and ask them first 'do you want to handle
it yourself' so it slightly increased.

135
00:13:43,441 --> 00:13:50,120
but the contribution of other companies
has not really increased over time.

136
00:13:50,581 --> 00:13:57,361
Rather it has disappeared. It is
unfortunate but it looks like paid

137
00:13:57,361 --> 00:14:01,461
contributors are more productive than
others.

138
00:14:05,081 --> 00:14:12,741
In particular with EDF, they do the work,
but with some lag and we are faster

139
00:14:12,741 --> 00:14:21,321
so they just reuse what we have done. I
want to talk to Raphaël to see

140
00:14:21,321 --> 00:14:23,441
how we can do better towards this.

141
00:14:26,221 --> 00:14:29,821
How did the sponsorship level evolve?

142
00:14:30,361 --> 00:14:35,120
We have a steady increase, which is
rather nice. It is not a huge amount but

143
00:14:35,120 --> 00:14:41,241
it is significant because we fund almost
80 hours per month.

144
00:14:43,481 --> 00:14:50,560
It is close to our first goal. We wanted
that amount to be able to sustain

145
00:14:50,560 --> 00:14:51,621
ourselves.

146
00:14:54,081 --> 00:15:03,461
If you look at the sponsors, we have a
few big ones, possibly one very big

147
00:15:03,940 --> 00:15:10,501
We can't give the name officially yet so
I won't.It will be a big jump in the graph

148
00:15:11,141 --> 00:15:15,561
A few gold and many small sponsors.

149
00:15:15,880 --> 00:15:23,060
I don't want to be dependent too much on
one big sponsor. I really prefer many

150
00:15:23,060 --> 00:15:28,461
sponsors who are doing small donations
but donations which are sustainable

151
00:15:28,461 --> 00:15:32,420
year after year because we are not here
for 1 year or 2.

152
00:15:32,940 --> 00:15:36,102
We want to do it over the long term.

153
00:15:40,301 --> 00:15:45,121
We have some figures about how many
hours have been funded since the start

154
00:15:47,501 --> 00:15:52,581
Feel free to interrupt me if you have any
questions. I can take them at any time

155
00:15:55,501 --> 00:16:01,141
That's it for evolution. Now, the future.

156
00:16:01,741 --> 00:16:04,221
What do we expect for the future?

157
00:16:04,721 --> 00:16:07,560
First keep doing what we have been up
to

158
00:16:07,721 --> 00:16:09,780
Keep supporting the current set of packages.

159
00:16:10,421 --> 00:16:13,761
But for wheezy long term support we
would really like to have more

160
00:16:13,961 --> 00:16:18,660
supported packages. A browser would
be nice for desktop deployment.

161
00:16:22,161 --> 00:16:29,921
Virtualization support is also important
for many companies so we should be

162
00:16:29,921 --> 00:16:31,880
able to support something here.

163
00:16:32,981 --> 00:16:39,662
Also we want to avoid some pitfalls that
we had with squeeze LTS.

164
00:16:40,241 --> 00:16:45,821
As you know LTS users are currently
required to add a separate source

165
00:16:45,821 --> 00:16:55,101
list entry with squeeze-lts repository. The
security.debian.org squeeze

166
00:16:55,101 --> 00:17:01,641
repository is unused. It should be
possible for the LTS team to

167
00:17:01,641 --> 00:17:06,521
continue using the same repository as
the security team once it no longer

168
00:17:06,521 --> 00:17:18,401
use it. This will be the topic of a BoF
next week on Tue at 1800.

169
00:17:25,901 --> 00:17:29,661
What's the problem with supporting the
current set of packages?

170
00:17:30,221 --> 00:17:36,221
For example, we have MySQL right now in
Squeeze LTS in version 5.1

171
00:17:36,221 --> 00:17:38,521
which is no longer supported by Oracle.

172
00:17:40,901 --> 00:17:45,121
We don't even know if it's affected by
security issues because

173
00:17:45,121 --> 00:17:48,660
Oracle doesn't give info on
non supported releases.

174
00:17:49,941 --> 00:17:53,622
This is a problem, we should consider
switching to a newer version,

175
00:17:53,622 --> 00:17:58,461
but newer versions involve library
transition, which is not really nice

176
00:17:58,461 --> 00:18:00,201
in Long Term Support releases.

177
00:18:02,261 --> 00:18:07,060
There is some work to do here if we want
to do something serious.

178
00:18:08,721 --> 00:18:12,120
And we have other problems, other
packages which are similar.

179
00:18:15,121 --> 00:18:18,741
From time to time, we backport, we take
newer upstream versions.

180
00:18:20,261 --> 00:18:23,101
We did that for wireshark for example.

181
00:18:24,621 --> 00:18:30,142
This is what I said before, the limited
scope sucks, we want to be able to

182
00:18:30,142 --> 00:18:33,241
support more packages and we need more
support for this.

183
00:18:38,501 --> 00:18:46,721
[Q] Speaking of wireshark, we switched to
Wheezy's version, so it's solved.

184
00:18:47,361 --> 00:18:50,121
[Raphael] Yes, exactly, that's what I just
said.

185
00:18:51,502 --> 00:18:53,001
It used to be a problem.

186
00:18:53,161 --> 00:18:56,482
That's a part I did no update since March.

187
00:19:03,561 --> 00:19:07,061
Additionally, the problem with a separate
repository is that

188
00:19:07,061 --> 00:19:09,535
there is a propagation delay to the
mirrors

189
00:19:09,535 --> 00:19:11,881
that we don't have with
security.debian.org.

190
00:19:15,541 --> 00:19:18,840
I will speak a bit of how the team works.

191
00:19:20,781 --> 00:19:24,061
Basically, the first step is triaging new
security issues.

192
00:19:24,061 --> 00:19:29,541
We have a list of CVEs that comes in and
there are added to a text file

193
00:19:29,541 --> 00:19:33,441
data/CVE/list.

194
00:19:34,600 --> 00:19:41,241
Someone dispatches those on source
packages and then someone else reviews

195
00:19:41,241 --> 00:19:45,761
status in each release.

196
00:19:46,421 --> 00:19:50,780
Then the LTS team reviews the status in
Squeeze and members of security team

197
00:19:50,780 --> 00:19:52,640
review status in Wheezy and Jessie.

198
00:19:53,781 --> 00:19:56,701
And then, we decide what we must do with
the package.

199
00:19:57,961 --> 00:20:01,302
Depending on the analysis, either we say

200
00:20:01,302 --> 00:20:08,421
"We need to prepare an update", so we add
it to data/dla-needed.txt

201
00:20:08,981 --> 00:20:12,281
and someone will have to take care of
preparing the update.

202
00:20:12,761 --> 00:20:18,042
Or we say "The issue does not apply, does
not affect the current version"

203
00:20:18,361 --> 00:20:23,762
or we ignore it because the package is
unsupported or because

204
00:20:23,762 --> 00:20:27,801
the issue is really minor, not severe
enough.

205
00:20:29,321 --> 00:20:33,861
Sometimes, it can be that the issue is
already fixed in Debian

206
00:20:33,861 --> 00:20:36,101
due to some maintainer choices.

207
00:20:40,161 --> 00:20:44,921
When we do this classification, we contact
the maintainer to keep him in the loop

208
00:20:44,921 --> 00:20:48,081
and offer him the possibility to help us.

209
00:20:49,160 --> 00:20:52,241
Here's what such a text file looks like.

210
00:20:54,441 --> 00:20:58,621
The bold line is the line that we are
adding when we have decided

211
00:20:58,621 --> 00:21:01,921
what we're doing with the packages.

212
00:21:03,281 --> 00:21:06,781
This is all in the Subversion repository
on Alioth.

213
00:21:10,262 --> 00:21:13,961
Then, someone has to prepare the security
update.

214
00:21:14,440 --> 00:21:20,181
Basically, looking for a patch, it's often
useful to be able to look up at

215
00:21:20,181 --> 00:21:23,501
RedHat or Ubuntu or upstream.

216
00:21:23,921 --> 00:21:27,021
Best case is upstream because there are
nice upstream who are providing

217
00:21:27,021 --> 00:21:28,902
patches also for older versions.

218
00:21:31,521 --> 00:21:35,381
Usually there are already patches
available,

219
00:21:35,600 --> 00:21:37,561
not always for the good version.

220
00:21:37,901 --> 00:21:40,101
Sometimes we have to backport it.

221
00:21:41,041 --> 00:21:47,521
Then we prepare an upload with this
+deb6uX suffix that we're using now

222
00:21:47,521 --> 00:21:50,221
for security updates, stable updates.

223
00:21:52,360 --> 00:21:56,441
This is a rather known territory for
package maintainers.

224
00:21:58,881 --> 00:22:01,521
This is the hardest part sometimes,

225
00:22:01,521 --> 00:22:05,241
testing the update, making sure the issue
is fixed.

226
00:22:08,461 --> 00:22:11,861
When tools have test suites, it's nice,

227
00:22:11,861 --> 00:22:14,381
but sometimes we have to set it up
ourselves.

228
00:22:16,900 --> 00:22:20,501
Sometimes it is too hard, so we tend to,
out of safety measure,

229
00:22:20,501 --> 00:22:22,962
to mail the mailing list and say

230
00:22:22,962 --> 00:22:26,741
"Ok, I've done my best, but please double
check"

231
00:22:29,120 --> 00:22:31,722
It doesn't happen often that we have
testers

232
00:22:31,722 --> 00:22:36,180
but some lts users are willing to test it
before ???

233
00:22:37,061 --> 00:22:39,101
It would be really nice if we had
more of those.

234
00:22:40,402 --> 00:22:42,600
And if we don't get any negative feedback,

235
00:22:42,600 --> 00:22:43,961
then we upload.

236
00:22:45,581 --> 00:22:49,181
Then, you have to send
an announce.

237
00:22:49,901 --> 00:22:53,002
We call that DLA for Debian LTS
Advisory.

238
00:22:53,861 --> 00:22:57,961
We have a little script
./bin/gen-DLA

239
00:22:57,961 --> 00:23:00,861
that generates the template mail
and you have to add

240
00:23:01,041 --> 00:23:03,002
a little bit of description

241
00:23:03,280 --> 00:23:06,001
and basically send it to
debian-lts-announce

242
00:23:06,222 --> 00:23:13,140
with a GPG signature of someone in the
DD or DM keyring.

243
00:23:16,301 --> 00:23:20,542
The process is rather open to
all maintainers

244
00:23:20,542 --> 00:23:26,161
even the write rights to the secure
testing repository

245
00:23:26,161 --> 00:23:28,861
where we have this data/DLA/list file

246
00:23:29,520 --> 00:23:32,101
is writable by all Debian Developpers on
Alioth

247
00:23:32,682 --> 00:23:39,241
so, really, the entrance barrier is
rather low for Debian Maintainers

248
00:23:40,001 --> 00:23:41,221
and Debian Developpers.

249
00:23:45,700 --> 00:23:49,760
There's a file which is automatically
updated when you generate this template

250
00:23:49,760 --> 00:23:55,081
and which will update the tracker on the
web pages

251
00:23:55,081 --> 00:24:01,741
because all this data is nicely browsable
on security-tracker.debian.org.

252
00:24:03,561 --> 00:24:08,021
And it's all linked from the package
tracker.

253
00:24:11,382 --> 00:24:15,860
That's it. If you have a few questions,
I'm here to answer you.

254
00:24:30,301 --> 00:24:39,541
[inaudible question]

255
00:24:39,541 --> 00:24:41,641
… library and operating server,

256
00:24:41,641 --> 00:24:48,861
so that you have same API for clients,
or same ABI for clients and

257
00:24:48,861 --> 00:24:51,541
a newer server that actually gets
security patches.

258
00:24:52,261 --> 00:24:55,521
[RH] In my head, yes, but we did not look
further yet.

259
00:24:55,800 --> 00:25:02,521
I really want to use the BOF that is
upcoming to discuss it

260
00:25:03,961 --> 00:25:11,220
As I said, the amount of funding we have
allows us right now

261
00:25:11,220 --> 00:25:15,621
to keep up with security update but
we do not have many spare cycles

262
00:25:16,041 --> 00:25:19,481
for dealing with such things.

263
00:25:20,320 --> 00:25:22,380
But it's slowly coming up,

264
00:25:22,380 --> 00:25:25,861
as I said there's potentially a new
platinum sponsor.

265
00:25:27,061 --> 00:25:32,221
We will have a few hours free to look
into this and I think

266
00:25:32,221 --> 00:25:34,121
it's probably the best solution

267
00:25:34,481 --> 00:25:37,481
but I don't know if it works in practice,
I have not tried it.

268
00:25:42,181 --> 00:25:47,061
And since LTS, the end of life of Squeeze
happens in February,

269
00:25:47,061 --> 00:25:51,682
it's not too far away so it's possibly
a nice solution

270
00:25:51,682 --> 00:25:55,181
because upgrading to a newer upstream
version is harder.

271
00:25:56,821 --> 00:26:00,122
[Q] I've heard ???
Freexian ???

272
00:26:00,122 --> 00:26:04,161
you have enough contributors
but you could do more work

273
00:26:04,161 --> 00:26:08,411
but you don't have the funding or
is it, like, you also need

274
00:26:08,411 --> 00:26:10,381
more developpers?

275
00:26:10,680 --> 00:26:12,701
[RH] A bit of both, actually,

276
00:26:18,581 --> 00:26:21,661
the web page has always been very clear
on the rules

277
00:26:22,680 --> 00:26:28,421
That means any Debian Developper who has
made security uploads on its own already

278
00:26:28,421 --> 00:26:31,701
can apply and join the program and
be funded.

279
00:26:33,461 --> 00:26:35,321
Fortunately, I did not have too many

280
00:26:35,321 --> 00:26:36,821
because it would have been hard,

281
00:26:36,821 --> 00:26:39,681
but it has been steadily growing over
the months.

282
00:26:41,080 --> 00:26:44,661
Last year, there was only Thorsten,
Holger and me,

283
00:26:45,481 --> 00:26:49,821
but in the meantime we got Ben Hutchings
who is helping us on the kernel

284
00:26:50,781 --> 00:26:52,081
and a few more.

285
00:26:53,301 --> 00:26:58,301
I'm always looking, trying to make sure
that we don't give too much money

286
00:26:58,301 --> 00:26:59,841
to a single person

287
00:27:00,241 --> 00:27:04,201
because it's Debian, there has been
??? in the past.

288
00:27:04,622 --> 00:27:06,761
I've been involved, I know it.

289
00:27:07,981 --> 00:27:14,221
I don't want anyone to have their life
depend on LTS work, really.

290
00:27:14,601 --> 00:27:17,841
And I don't want the opposite way also,
LTS to depend on a single person

291
00:27:18,001 --> 00:27:19,241
that can go away.

292
00:27:19,501 --> 00:27:26,161
I try to limit it to maximum 20 ???
per month for each person

293
00:27:26,161 --> 00:27:29,641
and right now, we're well below that
so it's good.

294
00:27:31,941 --> 00:27:37,460
By the way, most Debian Developpers are
currently european,

295
00:27:37,460 --> 00:27:39,761
the fact that everything is euro-based
is fine

296
00:27:40,561 --> 00:27:46,162
but one developer is american and it's
no problem to pay in dollars

297
00:27:46,881 --> 00:27:51,921
So the amount will vary with the rates
but it's not a problem.

298
00:27:52,561 --> 00:27:56,120
And even more, I'm surprised to have no
contributors

299
00:27:56,120 --> 00:28:02,181
in the countries where the rate of labor
is well bellow.

300
00:28:02,501 --> 00:28:07,041
I mean, if we have south american
contributors,

301
00:28:07,041 --> 00:28:10,421
or african or I don't know, I don't want
to stigmatize anyone,

302
00:28:10,421 --> 00:28:15,641
but if there are people who could live
for much more,

303
00:28:19,041 --> 00:28:22,180
if they would be payed 10 hours and
have made their month,

304
00:28:23,281 --> 00:28:25,522
they can still join, it's not a problem,

305
00:28:25,522 --> 00:28:30,061
it's not a high rate because we want only
europeans,

306
00:28:30,061 --> 00:28:34,042
it's because we want to allow everybody

307
00:28:34,042 --> 00:28:38,221
and if they can be payed for 10 hours
by Freexian and then spend

308
00:28:38,221 --> 00:28:41,201
the rest of the month doing free work
on Debian, the better.

309
00:28:43,400 --> 00:28:45,741
If you want to join the program,
get in touch,

310
00:28:46,362 --> 00:28:48,940
there are details on the webpages

311
00:28:48,940 --> 00:28:52,341
and the more people funded
the better.

312
00:28:58,181 --> 00:29:01,621
[Q] How do you or other developers
motivate yourself

313
00:29:01,621 --> 00:29:03,641
to do this kind of LTS work,

314
00:29:03,641 --> 00:29:07,222
because obviously it's important
for companies to have

315
00:29:07,222 --> 00:29:08,621
stable releases,

316
00:29:08,621 --> 00:29:13,922
but it also, to me at least, doesn't seem
very exciting from a technology standpoint

317
00:29:17,101 --> 00:29:20,321
[RH] Do you think it's exciting or
not exciting?

318
00:29:20,941 --> 00:29:22,021
I'm not sure I understood.

319
00:29:22,181 --> 00:29:23,220
Not exciting, ok.

320
00:29:23,361 --> 00:29:24,760
I agree, it's not exciting.

321
00:29:25,281 --> 00:29:27,281
[laughter]

322
00:29:28,141 --> 00:29:30,781
[inaudible question]

323
00:29:31,061 --> 00:29:32,460
[RH] A bit of both.

324
00:29:32,881 --> 00:29:35,781
I want Debian to succeed and I know that

325
00:29:35,781 --> 00:29:38,201
LTS support is important for Debian
to succeed

326
00:29:38,201 --> 00:29:41,681
so I want to make sure the project is
working well and alive

327
00:29:41,681 --> 00:29:45,401
but clearly, I'm doing it because…

328
00:29:46,761 --> 00:29:51,581
Let's say, the security support work,
providing security update,

329
00:29:51,581 --> 00:29:54,761
backporting code, it's for money and
because I need to live.

330
00:29:55,561 --> 00:30:00,740
But organizing the LTS project is
something I do for free

331
00:30:00,740 --> 00:30:03,200
because I want it for Debian

332
00:30:03,200 --> 00:30:04,481
because Debian needs it.

333
00:30:06,381 --> 00:30:07,981
[Q] I have a question.

334
00:30:08,222 --> 00:30:12,101
Who announces the public relation,
the marketing stuff of

335
00:30:12,101 --> 00:30:14,581
the Debian Long Term Support?

336
00:30:14,861 --> 00:30:18,781
Do you do it yourself or do you have some
professional help?

337
00:30:19,880 --> 00:30:21,741
[RH] I do not have any professional help.

338
00:30:22,461 --> 00:30:24,061
Basically, the way we…

339
00:30:25,660 --> 00:30:29,380
Well, we could do much better, I mean,
in number of sponsors.

340
00:30:29,920 --> 00:30:32,861
There are almost no US-based sponsors,

341
00:30:32,861 --> 00:30:35,361
there are lots of US companies
that could help.

342
00:30:38,202 --> 00:30:43,761
It's true, I'm fighting between my time

343
00:30:43,761 --> 00:30:46,641
spending free time on Debian,
doing LTS Debian.

344
00:30:49,121 --> 00:30:51,940
I contact a few companies that people
suggest me to contact,

345
00:30:51,940 --> 00:30:54,582
but I'm not doing much things
by myself.

346
00:30:56,041 --> 00:30:58,701
[Q] So, you would benefit a lot from
some professional help

347
00:30:58,701 --> 00:31:00,261
in the marketing department or

348
00:31:00,460 --> 00:31:01,041
[RH] Yes

349
00:31:01,181 --> 00:31:02,001
[Q] public relation

350
00:31:02,341 --> 00:31:06,921
[RH] Yes, but I don't have money
to fund this or not much.

351
00:31:07,421 --> 00:31:10,101
The 10€ difference is for
administrative cost.

352
00:31:10,882 --> 00:31:17,642
Maybe a bit for this kind of stuff but
it doesn't look like a lot for this.

353
00:31:19,660 --> 00:31:22,320
[Q] I just wanted to ask to the previous
questioner about

354
00:31:22,320 --> 00:31:27,521
finding motivation for providing patches
for LTS packages and in my view

355
00:31:27,521 --> 00:31:31,100
it's not very different from providing
security patches for stable.

356
00:31:32,400 --> 00:31:36,662
Sometimes it's a bit harder because the
software is a bit older

357
00:31:36,662 --> 00:31:41,041
but we overcame this situation
for wireshark for example

358
00:31:41,041 --> 00:31:46,221
by updating wireshark because it was
quite impossible to backport older

359
00:31:46,221 --> 00:31:47,801
security fixes.

360
00:31:50,261 --> 00:31:52,300
I think it's part of the maintainance.

361
00:31:52,960 --> 00:31:57,880
If you maintain a project, you should
maintain the security issues in stable

362
00:31:57,880 --> 00:32:01,661
and if you care a bit more and if you have
a little more time,

363
00:32:01,661 --> 00:32:05,221
you can care about the package in LTS
as well.

364
00:32:06,281 --> 00:32:09,781
I did it for wireshark for free, because
it's easy for me.

365
00:32:11,321 --> 00:32:12,761
[RH] I'm grateful, thank you.

366
00:32:16,561 --> 00:32:19,502
I want to point out that it's a good
thing that

367
00:32:19,502 --> 00:32:21,421
LTS and security are different,

368
00:32:21,421 --> 00:32:24,502
because we have different policies like
we said,

369
00:32:24,502 --> 00:32:27,521
importing a new upstream version is
something we can do

370
00:32:27,521 --> 00:32:30,581
when it doesn't break too much.

371
00:32:31,381 --> 00:32:35,581
The command line interface had not changed
so badly between both versions

372
00:32:35,581 --> 00:32:37,001
so it was possible.

373
00:32:37,361 --> 00:32:43,260
It's not possible for all projects, but we
are not so strict on new upstream versions

374
00:32:43,441 --> 00:32:47,521
And if you look at what others have been
doing, RedHat,

375
00:32:47,521 --> 00:32:52,681
they too import new upstream version
quite often, in fact,

376
00:32:52,681 --> 00:32:55,401
even on libraries like nss.

377
00:33:02,781 --> 00:33:06,801
[Q] Due to lack of browsers in LTS support
and Xen support

378
00:33:06,801 --> 00:33:10,862
was it an issue with companies you are
working on, who are sponsoring Freexian?

379
00:33:12,521 --> 00:33:14,101
[RH] I'm not sure I understood.

380
00:33:14,681 --> 00:33:21,441
[Q] The lack of browsers support and
Xen support in the LTS version,

381
00:33:21,441 --> 00:33:25,201
was that an issue with the companies
who you work with,

382
00:33:25,201 --> 00:33:27,181
who are sponsoring your work?

383
00:33:29,581 --> 00:33:31,181
[RH] Browsers, not so much.

384
00:33:31,561 --> 00:33:35,921
Most sponsors are hosters and
stuff like that

385
00:33:35,921 --> 00:33:40,401
but they were annoyed by the lack of
qemu or Xen support.

386
00:33:41,221 --> 00:33:45,661
They did upgrade their host machines
but not their guest machines

387
00:33:45,661 --> 00:33:47,882
for their customers.

388
00:33:48,741 --> 00:33:51,821
Obviously, that would be
the first priority to fix.

389
00:33:52,001 --> 00:33:57,901
Browser is nice, but it's also one of
those cases where we just need to

390
00:33:57,901 --> 00:33:59,641
integrate newer upstream versions

391
00:34:01,261 --> 00:34:03,381
??? doing it.

392
00:34:03,841 --> 00:34:08,300
And I was hoping for Raphael Geissert
to do it because he does it for

393
00:34:08,300 --> 00:34:09,860
Électricité de France.

394
00:34:11,781 --> 00:34:13,381
I'll catch him.

395
00:34:16,501 --> 00:34:17,561
Convince him.

396
00:34:18,981 --> 00:34:24,540
I know him quite well, he's working in
Lyon near me, not far away.

397
00:34:32,641 --> 00:34:38,321
[Q] Having sponsors for doing the LTS
work is a signal of

398
00:34:38,321 --> 00:34:41,461
the need for LTS versions,

399
00:34:41,461 --> 00:34:46,522
but do you have additional statistics on
the usage of LTS?

400
00:34:49,861 --> 00:34:51,621
[RH] Not really.

401
00:34:52,081 --> 00:34:56,201
Since LTS is not hosted in security
mirrors but on all mirrors,

402
00:34:56,201 --> 00:34:58,161
we don't have access to all the
statistics.

403
00:34:59,562 --> 00:35:05,401
But I know from mails, thank mails and
stuff like that

404
00:35:05,401 --> 00:35:08,621
that it's really appreciated and used
quite a lot,

405
00:35:08,621 --> 00:35:10,240
even from end users.

406
00:35:10,462 --> 00:35:14,381
There are users who like the antiquated
GNOME stuff.

407
00:35:16,961 --> 00:35:19,281
I'm fine with GNOME 3, by the way.

408
00:35:21,281 --> 00:35:25,441
Yes, it's really widely used and widely
appreciated.

409
00:35:34,261 --> 00:35:36,720
[Rhonda] Thanks for your attention.

410
00:35:44,401 --> 00:35:49,362
[Applause]
